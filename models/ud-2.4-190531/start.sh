#!/bin/sh

dir=$(dirname $0)
ud24_ack="http://ufal.mff.cuni.cz/udpipe/models#universal_dependencies_24_models_acknowledgements"

$dir/udpipe_server "$@" \
 english-ewt-ud-2.4-190531 \
 afrikaans-afribooms-ud-2.4-190531:af:afr         $dir/afrikaans-afribooms-ud-2.4-190531.udpipe $ud24_ack \
 ancient_greek-perseus-ud-2.4-190531:grc          $dir/ancient_greek-perseus-ud-2.4-190531.udpipe $ud24_ack \
 arabic-padt-ud-2.4-190531:ar:ara                 $dir/arabic-padt-ud-2.4-190531.udpipe $ud24_ack \
 armenian-armtdp-ud-2.4-190531:hy:hye:arm         $dir/armenian-armtdp-ud-2.4-190531.udpipe $ud24_ack \
 basque-bdt-ud-2.4-190531:eu:eus                  $dir/basque-bdt-ud-2.4-190531.udpipe $ud24_ack \
 belarusian-hse-ud-2.4-190531:be:bel              $dir/belarusian-hse-ud-2.4-190531.udpipe $ud24_ack \
 bulgarian-btb-ud-2.4-190531:bg:bul               $dir/bulgarian-btb-ud-2.4-190531.udpipe $ud24_ack \
 catalan-ancora-ud-2.4-190531:ca:cat              $dir/catalan-ancora-ud-2.4-190531.udpipe $ud24_ack \
 chinese-gsd-ud-2.4-190531:zh:zho:chi             $dir/chinese-gsd-ud-2.4-190531.udpipe $ud24_ack \
 classical_chinese-kyoto-ud-2.4-190531:lzh        $dir/classical_chinese-kyoto-ud-2.4-190531.udpipe $ud24_ack \
 coptic-scriptorium-ud-2.4-190531:cop             $dir/coptic-scriptorium-ud-2.4-190531.udpipe $ud24_ack \
 croatian-set-ud-2.4-190531:hr:hrv                $dir/croatian-set-ud-2.4-190531.udpipe $ud24_ack \
 czech-pdt-ud-2.4-190531:cs:ces:cze               $dir/czech-pdt-ud-2.4-190531.udpipe $ud24_ack \
 danish-ddt-ud-2.4-190531:da:dan                  $dir/danish-ddt-ud-2.4-190531.udpipe $ud24_ack \
 dutch-alpino-ud-2.4-190531:nl:nld:dut            $dir/dutch-alpino-ud-2.4-190531.udpipe $ud24_ack \
 english-ewt-ud-2.4-190531:en:eng                 $dir/english-ewt-ud-2.4-190531.udpipe $ud24_ack \
 estonian-edt-ud-2.4-190531:et:est                $dir/estonian-edt-ud-2.4-190531.udpipe $ud24_ack \
 finnish-tdt-ud-2.4-190531:fi:fin                 $dir/finnish-tdt-ud-2.4-190531.udpipe $ud24_ack \
 french-gsd-ud-2.4-190531:fr:fra:fre              $dir/french-gsd-ud-2.4-190531.udpipe $ud24_ack \
 galician-ctg-ud-2.4-190531:gl:glg                $dir/galician-ctg-ud-2.4-190531.udpipe $ud24_ack \
 german-gsd-ud-2.4-190531:de:deu:ger              $dir/german-gsd-ud-2.4-190531.udpipe $ud24_ack \
 gothic-proiel-ud-2.4-190531:got                  $dir/gothic-proiel-ud-2.4-190531.udpipe $ud24_ack \
 greek-gdt-ud-2.4-190531:el:ell:gre               $dir/greek-gdt-ud-2.4-190531.udpipe $ud24_ack \
 hebrew-htb-ud-2.4-190531:he:heb                  $dir/hebrew-htb-ud-2.4-190531.udpipe $ud24_ack \
 hindi-hdtb-ud-2.4-190531:hi:hin                  $dir/hindi-hdtb-ud-2.4-190531.udpipe $ud24_ack \
 hungarian-szeged-ud-2.4-190531:hu:hun            $dir/hungarian-szeged-ud-2.4-190531.udpipe $ud24_ack \
 indonesian-gsd-ud-2.4-190531:id:ind              $dir/indonesian-gsd-ud-2.4-190531.udpipe $ud24_ack \
 irish-idt-ud-2.4-190531:ga:gle                   $dir/irish-idt-ud-2.4-190531.udpipe $ud24_ack \
 italian-isdt-ud-2.4-190531:it:ita                $dir/italian-isdt-ud-2.4-190531.udpipe $ud24_ack \
 japanese-gsd-ud-2.4-190531:ja:jpn                $dir/japanese-gsd-ud-2.4-190531.udpipe $ud24_ack \
 korean-kaist-ud-2.4-190531:ko:kor                $dir/korean-kaist-ud-2.4-190531.udpipe $ud24_ack \
 latin-ittb-ud-2.4-190531:la:lat                  $dir/latin-ittb-ud-2.4-190531.udpipe $ud24_ack \
 latvian-lvtb-ud-2.4-190531:lv:lav                $dir/latvian-lvtb-ud-2.4-190531.udpipe $ud24_ack \
 lithuanian-alksnis-ud-2.4-190531:lt:lit          $dir/lithuanian-alksnis-ud-2.4-190531.udpipe $ud24_ack \
 maltese-mudt-ud-2.4-190531:mt:mlt                $dir/maltese-mudt-ud-2.4-190531.udpipe $ud24_ack \
 marathi-ufal-ud-2.4-190531:mr:mar                $dir/marathi-ufal-ud-2.4-190531.udpipe $ud24_ack \
 north_sami-giella-ud-2.4-190531:se:sme           $dir/north_sami-giella-ud-2.4-190531.udpipe $ud24_ack \
 norwegian-bokmaal-ud-2.4-190531:nb:nob           $dir/norwegian-bokmaal-ud-2.4-190531.udpipe $ud24_ack \
 norwegian-nynorsk-ud-2.4-190531:nn:nno           $dir/norwegian-nynorsk-ud-2.4-190531.udpipe $ud24_ack \
 old_church_slavonic-proiel-ud-2.4-190531:cu:chu  $dir/old_church_slavonic-proiel-ud-2.4-190531.udpipe $ud24_ack \
 old_french-srcmf-ud-2.4-190531:fro               $dir/old_french-srcmf-ud-2.4-190531.udpipe $ud24_ack \
 old_russian-torot-ud-2.4-190531:orv              $dir/old_russian-torot-ud-2.4-190531.udpipe $ud24_ack \
 persian-seraji-ud-2.4-190531:fa:fas:per          $dir/persian-seraji-ud-2.4-190531.udpipe $ud24_ack \
 polish-pdb-ud-2.4-190531:pl:pol                  $dir/polish-pdb-ud-2.4-190531.udpipe $ud24_ack \
 portuguese-gsd-ud-2.4-190531:pt:por              $dir/portuguese-gsd-ud-2.4-190531.udpipe $ud24_ack \
 romanian-rrt-ud-2.4-190531:ro:ron:rum            $dir/romanian-rrt-ud-2.4-190531.udpipe $ud24_ack \
 russian-syntagrus-ud-2.4-190531:ru:rus           $dir/russian-syntagrus-ud-2.4-190531.udpipe $ud24_ack \
 serbian-set-ud-2.4-190531:sr:srp                 $dir/serbian-set-ud-2.4-190531.udpipe $ud24_ack \
 slovak-snk-ud-2.4-190531:sk:slk:slo              $dir/slovak-snk-ud-2.4-190531.udpipe $ud24_ack \
 slovenian-ssj-ud-2.4-190531:sl:slv               $dir/slovenian-ssj-ud-2.4-190531.udpipe $ud24_ack \
 spanish-ancora-ud-2.4-190531:es:spa              $dir/spanish-ancora-ud-2.4-190531.udpipe $ud24_ack \
 swedish-talbanken-ud-2.4-190531:sv:swe           $dir/swedish-talbanken-ud-2.4-190531.udpipe $ud24_ack \
 tamil-ttb-ud-2.4-190531:ta:tam                   $dir/tamil-ttb-ud-2.4-190531.udpipe $ud24_ack \
 telugu-mtg-ud-2.4-190531:te:tel                  $dir/telugu-mtg-ud-2.4-190531.udpipe $ud24_ack \
 turkish-imst-ud-2.4-190531:tr:tur                $dir/turkish-imst-ud-2.4-190531.udpipe $ud24_ack \
 ukrainian-iu-ud-2.4-190531:uk:ukr                $dir/ukrainian-iu-ud-2.4-190531.udpipe $ud24_ack \
 urdu-udtb-ud-2.4-190531:ur:urd                   $dir/urdu-udtb-ud-2.4-190531.udpipe $ud24_ack \
 uyghur-udt-ud-2.4-190531:ug:uig                  $dir/uyghur-udt-ud-2.4-190531.udpipe $ud24_ack \
 vietnamese-vtb-ud-2.4-190531:vi:vie              $dir/vietnamese-vtb-ud-2.4-190531.udpipe $ud24_ack \
 wolof-wtb-ud-2.4-190531:wo:wol:wof               $dir/wolof-wtb-ud-2.4-190531.udpipe $ud24_ack \
