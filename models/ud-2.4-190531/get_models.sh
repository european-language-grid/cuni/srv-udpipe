#!/bin/sh

set -e

dir=$(dirname $0)

grep -o "[^/]*-ud-2.4-190531.udpipe" $dir/start.sh | while read model; do
  echo Downloading $model
  curl https://lindat.mff.cuni.cz/repository/xmlui/bitstream/handle/11234/1-2998/$model -o $dir/$model
done
