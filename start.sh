#!/bin/sh

dir=$(dirname $0)

$dir/udpipe_server/start.sh 8001 --daemon --log_file "" --concurrent_models 10 --no_check_models_loadable --max_connections 256 --max_request_size 1024
python3 $dir/elg_adapter/udpipe_elg_rest_server.py --port=8080 --max_request_size=1024000 --udpipe_host=localhost --udpipe_port=8001
