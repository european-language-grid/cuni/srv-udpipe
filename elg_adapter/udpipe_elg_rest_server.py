#!/usr/bin/env python3
import argparse
import json
import re

import aiohttp
import aiohttp.web

class UDPipeElgAdapter:
    class ProcessingError(Exception):
        def __init__(self, code, text, *params):
            self.code = code
            self.text = text
            self.params = params

        @staticmethod
        def InternalError(text):
            return UDPipeElgAdapter.ProcessingError("elg.service.internalError", "Internal error during processing: {0}", text)

        @staticmethod
        def InvalidRequest():
            return UDPipeElgAdapter.ProcessingError("elg.request.invalid", "Invalid request message")

        @staticmethod
        def TooLarge():
            return UDPipeElgAdapter.ProcessingError("elg.request.too.large", "Request size too large")

        @staticmethod
        def UnsupportedMime(mime):
            return UDPipeElgAdapter.ProcessingError("elg.request.text.mimeType.unsupported", "MIME type {0} not supported by this service", mime)

        @staticmethod
        def UnsupportedType(request_type):
            return UDPipeElgAdapter.ProcessingError("elg.request.type.unsupported", "Request type {0} not supported by this service", request_type)


    def __init__(self, host, port, max_request_size, session):
        self.host = host
        self.port = port
        self.max_request_size = max_request_size
        self.session = session

    def _error_message(self, message):
        return {
            "failure": {
                "errors": [{
                    "code": message.code,
                    "text": message.text,
                    "params": message.params,
                }]
            }
        }

    def _annotation_message(self, annotations):
        return {
            "response": {
                "type": "annotations",
                "annotations": {
                    "udpipe/{}".format(key): annotations[key] for key in annotations
                }
            }
        }

    async def _parse_elg_request(self, request):
        try:
            if request.content_type == "text/plain":
                return await request.text()
            elif request.content_type == "application/json":
                body = await request.text()
                try:
                    request = json.loads(body)
                except json.JSONDecodeError:
                    raise self.ProcessingError.InvalidRequest()

                if not isinstance(request, dict):
                    raise self.ProcessingError.InvalidRequest()

                if request.get("type", None) != "text":
                    raise self.ProcessingError.UnsupportedType(request.get("type", "<missing>"))

                if request.get("mimeType", "text/plain") != "text/plain":
                    raise self.ProcessingError.UnsupportedMime(request["mimeType"])

                return request.get("content", "")
            else:
                raise self.ProcessingError.UnsupportedMime(request.content_type)
        except aiohttp.web.HTTPRequestEntityTooLarge:
            raise self.ProcessingError.TooLarge()

    async def _run_udpipe(self, text, model):
        # Construct request
        payload = aiohttp.FormData({
            "data": text,
            "model": model,
            "tokenizer": "ranges",
            "tagger": "",
            "parser": "",
            "output": "conllu",
        })._gen_form_data() # This is awful, but we want to force multipart/form-data mime.
        # The multipart/form-data is more space efficient, because it uses UTF-8 encoding
        # directly, instead of escaping used by x-www-form-urlencoded.

        # Make sure the request is not too large
        if payload.size > args.max_request_size:
            raise self.ProcessingError.TooLarge()

        # Perform the request
        try:
            async with self.session.post("http://{}:{}/process".format(self.host, self.port), data=payload) as request:
                response = await request.json()
                response = response["result"]
        except:
            raise self.ProcessingError.InternalError("Cannot run the UDPipe service")

        return response

    def _conllu_parse_word(self, cols):
        result = {"form": cols[1]}
        if cols[2] != "_" or cols[1] == "_": result["lemma"] = cols[2]
        if cols[3] != "_": result["upos"] = cols[3]
        if cols[4] != "_": result["xpos"] = cols[4]
        if cols[5] != "_": result["feats"] = cols[5]
        if cols[6] != "_": result["head"] = int(cols[6])
        if cols[7] != "_": result["deprel"] = cols[7]
        return result

    def _conllu_to_elg_annotations(self, conllu):
        annots = {"paragraphs": [], "sentences": [], "tokens": []}

        first_par, first_sent, last_token, in_mwt = None, None, None, 0
        for line in conllu.split("\n"):
            line = line.rstrip("\r")

            # Handle continuing multi-word projects
            if in_mwt:
                cols = line.split("\t")
                if len(cols) != 10 or not cols[0].isdigit():
                    raise self.ProcessingError("Internal error: Cannot parse CoNLL-U response")
                annots["tokens"][-1]["features"]["words"].append(self._conllu_parse_word(cols))
                in_mwt -= 1
                continue

            if line.startswith("#"):
                # Check end of paragraph
                if "newpar" in line:
                    if first_par is not None and last_token is not None:
                        annots["paragraphs"].append({"start": first_par, "end": last_token, "features": {}})
                    first_par = None
                continue

            if not line:
                # Check end of sentence
                if first_sent is not None and last_token is not None:
                    annots["sentences"].append({"start": first_sent, "end": last_token, "features": {}})
                first_sent = None
                continue

            cols = line.split("\t")
            if len(cols) != 10:
                raise self.ProcessingError("Internal error: Cannot parse CoNLL-U response")

            # Ignore enhanced UD nodes
            if "." in cols[0]:
                continue

            tokens = None
            match = re.match(r"(\d+)-(\d+)$", cols[0])
            if match:
                # Muli-word token
                first, last = int(match.group(1)), int(match.group(2))
                tokens = last - first + 1
            elif cols[0].isdigit():
                tokens = 1
            else:
                raise self.ProcessingError("Internal error: Cannot parse CoNLL-U response")

            match = re.search(r"TokenRange=(\d+):(\d+)", cols[9])
            if not match:
                raise self.ProcessingError("Internal error: Cannot parse CoNLL-U response")

            token_start, token_end = int(match.group(1)), int(match.group(2))
            if first_par is None:
                first_par = token_start
            if first_sent is None:
                first_sent = token_start
            last_token = token_end

            annots["tokens"].append({"start": token_start, "end": token_end, "features": {"words": []}})
            if tokens == 1:
                annots["tokens"][-1]["features"]["words"].append(self._conllu_parse_word(cols))
            else:
                in_mwt = tokens

        if first_par is not None and last_token is not None:
            annots["paragraphs"].append({"start": first_par, "end": last_token, "features": {}})
        if first_sent is not None and last_token is not None:
            annots["sentences"].append({"start": first_sent, "end": last_token, "features": {}})

        return annots

    def _json_response(self, json):
        return aiohttp.web.json_response(json, headers={"Access-Control-Allow-Origin": "*"})

    async def process_elg_request(self, request):
        model = request.match_info.get("model", "")
        try:
            content = await self._parse_elg_request(request)
            conllu = await self._run_udpipe(content, model)
            annotations = self._conllu_to_elg_annotations(conllu)
            return self._json_response(self._annotation_message(annotations))

        except self.ProcessingError as e:
            return self._json_response(self._error_message(e))
        except:
            return self._json_response(self._error_message(self.ProcessingError.InternalError("Internal service error")))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--max_request_size", default=1000000, type=int, help="Maximum request size")
    parser.add_argument("--port", default=8080, type=int, help="Port where to start the REST server")
    parser.add_argument("--udpipe_host", default="localhost", type=str, help="Host of running UDPipe server")
    parser.add_argument("--udpipe_port", default=8001, type=int, help="Port of running UDPipe server")
    args = parser.parse_args()

    async def server(args):
        session = aiohttp.ClientSession()
        adapter = UDPipeElgAdapter(args.udpipe_host, args.udpipe_port, args.max_request_size, session)

        # Allow triple the limit in getting the request -- it might use %XY encoding.
        server = aiohttp.web.Application(client_max_size=3 * args.max_request_size)
        server.add_routes([aiohttp.web.post("/udpipe/{model}", adapter.process_elg_request)])
        server.on_cleanup.append(lambda app: session.close())

        return server

    aiohttp.web.run_app(server(args), port=args.port)
