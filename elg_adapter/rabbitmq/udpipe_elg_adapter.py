#!/usr/bin/env python3
import asyncio
import collections
import json
import re
import urllib.parse

class UDPipeElgAdapter:
    class ProcessingError(Exception):
        def __init__(self, message):
            self.message = message

    class MetadataHolder:
        def __init__(self, metadata=None):
            self.metadata = metadata

    def __init__(self, host, port, model):
        self.host = host
        self.port = port
        self.model = model

    def _error_message(self, metadata_holder, message):
        return json.dumps({
            "metadata": metadata_holder.metadata,
            "failure": {
                "errors": [
                    message
                ]
            }
        })

    def _annotation_message_message(self, metadata_holder, annotations):
        return json.dumps({
            "metadata": metadata_holder.metadata,
            "response": {
                "type": "annotations",
                "annotations": {
                    "udpipe/{}".format(key): annotations[key] for key in annotations
                }
            }
        })

    def _parse_elg_request(self, metadata_holder, request):
        try:
            request = json.loads(request)
        except json.JSONDecodeError:
            raise self.ProcessingError("Cannot parse JSON request")

        if not isinstance(request, dict):
            raise self.ProcessingError("The JSON request is not an object")

        if "metadata" in request:
            metadata_holder.metadata = request["metadata"]

        if "request" not in request or not isinstance(request["request"], dict):
            raise self.ProcessingError("No request payload found in the JSON request")
        request = request["request"]

        if request.get("type", None) != "text":
            raise self.ProcessingError("Only 'text' requests are handled by this service")

        if "content" not in request:
            raise self.ProcessingError("The JSON request is missing the 'content'")

        if request.get("mimeType", "text/plain") != "text/plain":
            raise self.ProcessingError("Unknown mimeType, only 'text/plain' supported")

        return request["content"]

    async def _run_udpipe(self, text):
        # Construct request
        payload = urllib.parse.urlencode({
            "data": text,
            "model": self.model,
            "tokenizer": "ranges",
            "tagger": "",
            "parser": "",
            "output": "conllu",
        })
        request = "\r\n".join([
            "POST /process HTTP/1.1",
            "Host: {}".format(self.host),
            "Content-Type: application/x-www-form-urlencoded",
            "Content-Length: {}".format(len(payload)),
            "",
            payload,
        ]).encode()

        # Asynchronously perform the request
        reader, writer = await asyncio.open_connection(self.host, self.port)
        writer.write(request)

        # Read the response
        in_header, response = True, []
        async for line in reader:
            line = line.decode().rstrip("\r\n")
            if not in_header:
                response.append(line)
            if not line:
                in_header = False

        response = json.loads("\n".join(response))
        return response["result"]

    def _conllu_parse_word(self, cols):
        result = {"form": cols[1]}
        if cols[2] != "_" or cols[1] == "_": result["lemma"] = cols[2]
        if cols[3] != "_": result["upos"] = cols[3]
        if cols[4] != "_": result["xpos"] = cols[4]
        if cols[5] != "_": result["feats"] = cols[5]
        if cols[6] != "_": result["head"] = int(cols[6])
        if cols[7] != "_": result["deprel"] = cols[7]
        return result

    def _conllu_to_elg_annotations(self, conllu):
        annots = {"paragraphs": [], "sentences": [], "tokens": []}

        first_par, first_sent, last_token, in_mwt = None, None, None, 0
        for line in conllu.split("\n"):
            line = line.rstrip("\r")

            # Handle continuing multi-word projects
            if in_mwt:
                cols = line.split("\t")
                if len(cols) != 10 or not cols[0].isdigit():
                    raise self.ProcessingError("Internal error: Cannot parse CoNLL-U response")
                annots["tokens"][-1]["features"]["words"].append(self._conllu_parse_word(cols))
                in_mwt -= 1
                continue

            if line.startswith("#"):
                # Check end of paragraph
                if "newpar" in line:
                    if first_par is not None and last_token is not None:
                        annots["paragraphs"].append({"start": first_par, "end": last_token, "features": None})
                    first_par = None
                continue

            if not line:
                # Check end of sentence
                if first_sent is not None and last_token is not None:
                    annots["sentences"].append({"start": first_sent, "end": last_token, "features": None})
                first_sent = None
                continue

            cols = line.split("\t")
            if len(cols) != 10:
                raise self.ProcessingError("Internal error: Cannot parse CoNLL-U response")

            # Ignore enhanced UD nodes
            if "." in cols[0]:
                continue

            tokens = None
            match = re.match(r"(\d+)-(\d+)$", cols[0])
            if match:
                # Muli-word token
                first, last = int(match.group(1)), int(match.group(2))
                tokens = last - first + 1
            elif cols[0].isdigit():
                tokens = 1
            else:
                raise self.ProcessingError("Internal error: Cannot parse CoNLL-U response")

            match = re.search(r"TokenRange=(\d+):(\d+)", cols[9])
            if not match:
                raise self.ProcessingError("Internal error: Cannot parse CoNLL-U response")

            token_start, token_end = int(match.group(1)), int(match.group(2))
            if first_par is None:
                first_par = token_start
            if first_sent is None:
                first_sent = token_start
            last_token = token_end

            annots["tokens"].append({"start": token_start, "end": token_end, "features": {"words": []}})
            if tokens == 1:
                annots["tokens"][-1]["features"]["words"].append(self._conllu_parse_word(cols))
            else:
                in_mwt = tokens

        if first_par is not None and last_token is not None:
            annots["paragraphs"].append({"start": first_par, "end": last_token, "features": None})
        if first_sent is not None and last_token is not None:
            annots["sentences"].append({"start": first_sent, "end": last_token, "features": None})

        return annots

    async def process_elg_request(self, request):
        metadata_holder = self.MetadataHolder()
        try:
            content = self._parse_elg_request(metadata_holder, request)
            try:
                conllu = await self._run_udpipe(content)
            except:
                raise self.ProcessingError("Internal error: Cannot run the UDPipe service")
            annotations = self._conllu_to_elg_annotations(conllu)
            return self._annotation_message_message(metadata_holder, annotations)

        except self.ProcessingError as e:
            return self._error_message(metadata_holder, e.message)
        except:
            return self._error_message(metadata_holder, "Internal error")

    async def process_elg_request_print(self, request):
        result = await self.process_elg_request(request)
        print(result)
        return result

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("request", type=str, help="Request to use")
    parser.add_argument("--host", default="localhost", type=str, help="Host of running UDPipe server")
    parser.add_argument("--model", default="en", type=str, help="UDPipe model to use")
    parser.add_argument("--port", default=8001, type=int, help="Port of running UDPipe server")
    args = parser.parse_args()

    adapter = UDPipeElgAdapter(args.host, args.port, args.model)
    asyncio.get_event_loop().run_until_complete(adapter.process_elg_request_print(args.request))
