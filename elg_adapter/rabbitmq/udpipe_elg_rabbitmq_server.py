#!/usr/bin/env python3
import argparse
import asyncio
import traceback

import aiormq
import yaml
import yarl

import udpipe_elg_adapter

parser = argparse.ArgumentParser()
parser.add_argument("--host", default="localhost", type=str, help="Host of running UDPipe server")
parser.add_argument("--model", default="en", type=str, help="UDPipe model to use")
parser.add_argument("--port", default=8001, type=int, help="Port of running UDPipe server")
args = parser.parse_args()

adapter = udpipe_elg_adapter.UDPipeElgAdapter(args.host, args.port, args.model)

async def on_message(message):
    try:
        request = message.body.decode()

        response = await adapter.process_elg_request(request)

        await message.channel.basic_publish(
            response.encode(),
            routing_key=message.header.properties.reply_to,
            properties=aiormq.spec.Basic.Properties(correlation_id=message.header.properties.correlation_id, content_type="application/json"),
        )

        await message.channel.basic_ack(message.delivery.delivery_tag)

    except:
        print("Got exception during message processing, ignoring.", traceback.format_exc(), flush=True)
        pass

async def serve():
    # Load ELG credentials
    with open("/etc/elg/mq.yaml", "r") as mq_yaml:
        credentials = yaml.safe_load(mq_yaml.read())

    # Open connection to RabbitMQ
    connection = await aiormq.connect(yarl.URL.build(
        scheme="amqp", host=credentials["host"], port=credentials["port"],
        user=credentials["username"], password=credentials["password"]))
    channel = await connection.channel()

    # Start the server
    await channel.basic_consume(credentials["topic_in"], on_message)

loop = asyncio.get_event_loop()
loop.run_until_complete(serve())
loop.run_forever()
