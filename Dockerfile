# Use an official Python 3.7 runtime with slim Debian Buster
FROM python:3.7-slim-buster

# Set the working directory to /udpipe
WORKDIR /udpipe

# Copy the required files to /udpipe
COPY LICENSE README.md start.sh /udpipe/
COPY elg_adapter /udpipe/elg_adapter/
COPY udpipe_server models/ud-2.4-190531 /udpipe/udpipe_server/

# Download the models
RUN apt-get update
RUN apt-get install -y curl
RUN /udpipe/udpipe_server/get_models.sh

# Install required Python packages
RUN pip3 install aiohttp

# Expose the service port
EXPOSE 8080

# Run the service when the container launches
CMD ["/bin/sh", "/udpipe/start.sh"]
